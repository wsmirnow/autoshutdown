#!/bin/bash
#
#set -x

. /etc/sysconfig/autoshutdown
. /usr/libexec/autoshutdown/ip-utils.sh

logit()
{
	# logger -p local0.notice -s -- AutoShutdown: $*
	echo AutoShutdown: $*
}

IsOnline()
{
        for i in $*; do
		ping $i -c1
		if [ "$?" == "0" ]; then
		  logit PC $i is still active, auto shutdown terminated
		  return 1
		fi
        done

	return 0
}

IsRunning()
{
        for i in $*; do
		if [ `pgrep -c $i` -gt 0 ] ; then
		  logit $i still active, auto shutdown terminated
                  return 1
                fi
        done

        return 0
}

IsDamonActive()
{
        for i in $*; do
                if [ `pgrep -c $i` -gt 1 ] ; then
                  logit $i still active, auto shutdown terminated
                  return 1
                fi
        done

        return 0
}

IsPortInUse()
{
        for i in $*; do
                LANG=C netstat -an | grep -q "${myIp}:${i}.*ESTABLISHED$"
                Err=${?}
                if [ ${Err} -eq 0 ] ; then
                  logit "Port ${i} is still in use, auto shutdown terminated"
                  return 1
                fi
        done

        return 0
}

IsBusy()
{
	# Samba
	if [ "x$SAMBANETWORK" != "x" ]; then
		if [ "auto" == "${SAMBANETWORK,,}" ]; then
			SAMBANETWORK="$(GetMyIp)"
			if [ "0" == "$?" ]; then
				SAMBANETWORK="${SAMBANETWORK%.*}."
			else
				SAMBANETWORK="auto"
			fi
		fi
		if [ "auto" != "$SAMBANETWORK" ]; then
			for smbNet in $SAMBANETWORK; do
				if [ `/usr/bin/smbstatus -b | grep $smbNet | wc -l ` != "0" ]; then
					logit samba connected, auto shutdown terminated
		  			return 1
				fi
			done
		fi
	fi

	#damons that always have one process running
	IsDamonActive $DAMONS
        if [ "$?" == "1" ]; then
                return 1
        fi

	#backuppc, wget, wsus, ....
        IsRunning $APPLICATIONS
	if [ "$?" == "1" ]; then
        	return 1
        fi

        # check network-ports
        if [ "x${NETWORKPORTS}" != "x" ]; then
                for myIp in $(GetMyIp); do
			IsPortInUse ${NETWORKPORTS}
			if [ "$?" == "1" ]; then
				return 1
			fi
		done
        fi

	# Read logged users
	USERCOUNT=`who | wc -l`;
	# No Shutdown if there are any users logged in
	test $USERCOUNT -gt 0 && { logit some users still connected, auto shutdown terminated; return 1; }

        IsOnline $CLIENTS
        if [ "$?" == "1" ]; then
                return 1
        fi

	return 0
}

RunShutdownCommand()
{	
	if [ "${SIMULATE_SHUTDOWN,,}" == "yes" ] || [ "${SIMULATE_SHUTDOWN,,}" == "true" ]; then
		logit "AutoShutdown would now shutdown, simulate..."
	else
		shutdowncommand=""
		case "x${SHUTDOWN_COMMAND,,}" in
		"xshutdown"|"xpoweroff") shutdowncommand="systemctl poweroff" ;;
		"xhibernate") shutdowncommand="systemctl suspend";;
		"xsuspend") shutdowncommand="systemctl suspend";;
		"x") shutdowncommand="systemctl suspend";;
		esac
		
		logit "AutoShutdown will shutdown now"
		sync && $shutdowncommand
	fi
}


#######################################################################

NOT_BUSY_COUNT="0"
while [ "${AUTO_SHUTDOWN,,}" == "true" ] || [ "${AUTO_SHUTDOWN,,}" == "yes" ]; do
	sleep $TEST_INTERVAL
	IsBusy
	if [ "$?" == "0" ]; then
		NOT_BUSY_COUNT=$((NOT_BUSY_COUNT +1))
		
		if [ "$NOT_BUSY_COUNT" == "$TEST_ITERATIONS" ]; then
			NOT_BUSY_COUNT="0"
			RunShutdownCommand
		fi
	fi
done

