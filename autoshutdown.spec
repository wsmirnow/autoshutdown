Name:		autoshutdown
Version:	1.0
Release:	3%{?dist}
Summary:	Autoshutdown systemd service

Group:		System Environment/Base
License:	MIT
URL:		https://bitbucket.org/wsmirnow/autoshutdown

Source0:	autoshutdown.conf
Source1:	autoshutdown.service
Source2:	autoshutdown.sh
Source3:	ip-utils.sh
Source4:	LICENSE

BuildRoot:	%{_tmppath}/%{name}-%{version}-root
BuildArch:	noarch

BuildRequires:	systemd

%description
Autoshutdown service shutdown/suspend/hibernate your comuter if it is idle for some time. 
Idle state will be tested by a script, where you can configure the test interval, test iterations, 
ports for inactivity test, services and running programms.


%prep
%setup -q


%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{_unitdir}
install -p -m 644 autoshutdown.service %{buildroot}%{_unitdir} 
mkdir -p %{buildroot}%{_libexecdir}/%{name}
install -p -m 755 *.sh %{buildroot}%{_libexecdir}/%{name}
mkdir -p %{buildroot}%{_sysconfdir}/sysconfig
install -p -m 644 autoshutdown.conf %{buildroot}%{_sysconfdir}/sysconfig/autoshutdown


%files
%{_unitdir}/*
%{_libexecdir}/*
%{_sysconfdir}/sysconfig/*


%post
%systemd_post autoshutdown.service


%preun
%systemd_preun autoshutdown.service


%postun
%systemd_postun autoshutdown.service


%changelog
* Sun Mar 20 2016 Waldemar Smirnow <waldemar.smirnow@gmail.com> 1.0-3
- spec file updated for builds on copr
* Sun Mar 20 2016 Waldemar Smirnow <waldemar.smirnow@gmail.com> 1.0-2
- license file added
* Sun Mar 20 2016 Waldemar Smirnow <waldemar.smirnow@gmail.com> 1.0-1
- first release
