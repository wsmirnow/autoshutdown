#!/bin/sh

GetNicNames() 
{
	devices=()
	for devstate in $(LANG=C nmcli -t -f DEVICE,STATE d | grep connected); do
		dev=${devstate%:connected}
		devices=("${devices[@]}" "$dev")
	done
	echo $devices
}

GetNicIp()
{
	for dev in $*; do
		devip=()
		for devipnet in $(LANG=C nmcli c show enp0s25 | grep IP4.ADDRESS | awk '{ print $2 }'); do
			devip=("${devip[@]}" "${devipnet%/*}")
		done
		echo ${devip[@]}
	done
}

GetMyIp()
{
	devices=$(GetNicNames)
	myip=$(GetNicIp $devices)
	echo ${myip[@]}
}
